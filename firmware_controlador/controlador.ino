#include <BLEDevice.h>
#include <BLEUtils.h>
#include <BLEServer.h>
#include <string.h>
#include <ctime>
#include <SD.h>
#include <SPI.h>

#define DEBUG 1

#if DEBUG == 1
#define DEBUG_PRINT(x) Serial.print(x)
#define DEBUG_PRINTLN(x) Serial.println(x)
#else
#define DEBUG_PRINT(x)
#define DEBUG_PRINTLN(x)
#endif

#define SERVICE_UUID "4fafc201-1fb5-459e-8fcc-c5c9c331914b"
#define CHARACTERISTIC_UUID "beb5483e-36e1-4688-b7f5-ea07361b26a8" // Ligar
#define SERVICE_UUID3 "4fafc201-1fb5-459e-8fcc-d5a9c331737b"
#define CHARACTERISTIC_UUID3 "beb5487e-36e1-4688-b7f5-ea07361b79c1"
#define SERVICE_UUID4 "4fafc201-1fb5-459e-8fcc-d5f9d331228a"
#define CHARACTERISTIC_UUID4 "beb5418e-36a1-4688-b7f5-ea07361b68d1"
#define SERVICE_UUID5 "4fafc201-1fb5-459e-8fcc-d5f9d331229b"
#define CHARACTERISTIC_UUID5 "beb5418e-36a1-4688-b7f5-ea07361b68a2"

#define BOTAO_LIGAR 14
#define BOTAO_DESLIGAR 14
#define BOTAO_ALTERAR_MODO 33
#define BOTAO_MAIS_POTENCIA 27
#define BOTAO_MENOS_POTENCIA 32

#define RX2 16
#define TX2 17

#define SD_CS_PIN 5 // Defina o pino de chip select do SD card

enum ESTADO_FOGAO {
  DESLIGADO = 0,
  LIGADO = 1,
  COZINHANDO = 2
};

enum NIVEIS_POTENCIAS {
  NENHUM_NIVEL_SELECIONADO = 0,
  NIVEL_1 = 1,
  NIVEL_2 = 2,
  NIVEL_3 = 3,
  NIVEL_4 = 4,
  NIVEL_5 = 5,
  NIVEL_6 = 6,
  NIVEL_7 = 7,
  NIVEL_8 = 8,
};

struct DadoHist {
  char pow[5], time[6];
};

// Variáveis globais
uint nivelPotenciaAtual = NENHUM_NIVEL_SELECIONADO;
ESTADO_FOGAO estadoFogao = DESLIGADO;
unsigned long tempoFinal = 0;
uint minutosIniciais = 0;
uint segundosIniciais = 0;
uint minutosAtuais = 0;
uint segundosAtuais = 0;

// Variáveis BLE globais
BLEServer *pServer;
BLEService *pService;
BLECharacteristic *pCharacteristic;
BLEService *historyService;
BLECharacteristic *char2;
BLEService *workingService;
BLECharacteristic *char3;
BLEService *routineService;
BLECharacteristic *char4;
BLEService *newService;
BLECharacteristic *newCharacteristic;

// Declaração das funções
void apertar_botao(int pino);
void ligar_fogao();
void desligar_fogao();
void alterar_potencia_para(uint novaPotencia);
void iniciar_cozimento(uint potencia, uint minutos, uint segundos);
void parar_cozimento();
void atualizar_tempo_cozimento();

class MyServerCallbacks : public BLEServerCallbacks {
  void onConnect(BLEServer *pServer) {
    Serial.println("CONNECTED...");
  }

  void onDisconnect(BLEServer *pServer) {
    Serial.println("DISCONNECTED...");
  }
};

class MyCharacteristicCallbacks : public BLECharacteristicCallbacks {
  void onWrite(BLECharacteristic *pCharacteristic) {
    String valor = pCharacteristic->getValue();
    Serial.print("Characteristic value written: ");
    Serial.println(valor.c_str());

    if (pCharacteristic->getUUID().toString() == CHARACTERISTIC_UUID) {
      if (valor.startsWith("d")) {
        parar_cozimento();
      } else {
        int potencia, minutos, segundos;
        if (sscanf(valor.c_str(), "%d|%d:%d", &potencia, &minutos, &segundos) == 3) {
          iniciar_cozimento(potencia, minutos, segundos);
        } else {
          DEBUG_PRINTLN("Formato de comando inválido.");
        }
      }
    } else if (pCharacteristic->getUUID().toString() == CHARACTERISTIC_UUID3) {
      Serial.println("Característica 3 alterada");
      // Adicione aqui o código para tratar a alteração da característica 3
    } else if (pCharacteristic->getUUID().toString() == CHARACTERISTIC_UUID4) {
      Serial.println("Característica 4 alterada");
      // Adicione aqui o código para tratar a alteração da característica 4
      File receitaFile = SD.open("/receita.txt", FILE_WRITE);
      if (receitaFile) {
        receitaFile.println(valor);
        receitaFile.close();
        Serial.println("Receita salva no SD card.");
      } else {
        Serial.println("Erro ao abrir o arquivo de receita.");
      }
    } else if (pCharacteristic->getUUID().toString() == CHARACTERISTIC_UUID5) { // Potencia
      alterar_potencia_para(valor.toInt());
    }
  }
};

// Função de configuração inicial
// Esta função configura o ambiente de execução, inicializando os pinos,
// o cartão SD, o BLE e as características de serviço.
void setup() {
  Serial.begin(115200);
  Serial2.begin(115200, SERIAL_8N1, RX2, TX2);

  pinMode(BOTAO_LIGAR, OUTPUT);
  pinMode(BOTAO_DESLIGAR, OUTPUT);
  pinMode(BOTAO_ALTERAR_MODO, OUTPUT);
  pinMode(BOTAO_MAIS_POTENCIA, OUTPUT);
  pinMode(BOTAO_MENOS_POTENCIA, OUTPUT);

  digitalWrite(BOTAO_LIGAR, HIGH);
  digitalWrite(BOTAO_DESLIGAR, HIGH);
  digitalWrite(BOTAO_ALTERAR_MODO, HIGH);
  digitalWrite(BOTAO_MAIS_POTENCIA, HIGH);
  digitalWrite(BOTAO_MENOS_POTENCIA, HIGH);

  if (!SD.begin(SD_CS_PIN)) {
    Serial.println("Falha na inicialização do cartão SD!");
    return;
  }
  Serial.println("Cartão SD inicializado com sucesso.");

  BLEDevice::init("fogao");

  pServer = BLEDevice::createServer();
  pServer->setCallbacks(new MyServerCallbacks());

  pService = pServer->createService(SERVICE_UUID);
  pCharacteristic = pService->createCharacteristic(
    CHARACTERISTIC_UUID,
    BLECharacteristic::PROPERTY_READ | BLECharacteristic::PROPERTY_WRITE);
  pCharacteristic->setCallbacks(new MyCharacteristicCallbacks());
  pCharacteristic->setValue("00:00|0");
  pService->start();

  workingService = pServer->createService(SERVICE_UUID3);
  char3 = workingService->createCharacteristic(
    CHARACTERISTIC_UUID3,
    BLECharacteristic::PROPERTY_READ | BLECharacteristic::PROPERTY_WRITE);
  char3->setCallbacks(new MyCharacteristicCallbacks());
  char3->setValue("00:00|0");
  workingService->start();

  routineService = pServer->createService(SERVICE_UUID4);
  char4 = routineService->createCharacteristic(
    CHARACTERISTIC_UUID4,
    BLECharacteristic::PROPERTY_READ | BLECharacteristic::PROPERTY_WRITE);
  char4->setCallbacks(new MyCharacteristicCallbacks());

  File receitaFile = SD.open("/receita.txt", FILE_READ);
  if (!receitaFile) {
    Serial.println("Arquivo de receita não encontrado. Criando arquivo.");
    receitaFile = SD.open("/receita.txt", FILE_WRITE);
    if (receitaFile) {
      receitaFile.println(""); // Escreve uma linha vazia
      receitaFile.close();
      char4->setValue(""); // Define a característica como vazia
    } else {
      Serial.println("Erro ao criar o arquivo de receita.");
    }
  } else {
    String receita = "";
    while (receitaFile.available()) {
      receita += (char)receitaFile.read();
    }
    char4->setValue(receita.c_str());
    receitaFile.close();
    Serial.println("Receita carregada do SD card.");
  }
  routineService->start();

  newService = pServer->createService(SERVICE_UUID5);
  newCharacteristic = newService->createCharacteristic(
    CHARACTERISTIC_UUID5,
    BLECharacteristic::PROPERTY_READ | BLECharacteristic::PROPERTY_WRITE);
  newCharacteristic->setCallbacks(new MyCharacteristicCallbacks());
  newCharacteristic->setValue("");
  newService->start();

  BLEAdvertising *pAdvertising = pServer->getAdvertising();
  pAdvertising->addServiceUUID(SERVICE_UUID);
  pAdvertising->start();

  Serial.println("Characteristic defined! Now you can read it in your phone!");

  DEBUG_PRINTLN("Setup completo. Pronto para receber comandos.");
}

// Função principal do loop
// Esta função atualiza continuamente o tempo de cozimento e processa
// comandos recebidos via Serial.
void loop() {
  String tempoPotAtual = "";
  if (minutosAtuais < 10) {
    tempoPotAtual += "0" + String(minutosAtuais);
  } else {
    tempoPotAtual += String(minutosAtuais);
  }

  tempoPotAtual += ":";

  if (segundosAtuais < 10) {
    tempoPotAtual += "0" + String(segundosAtuais);
  } else {
    tempoPotAtual += String(segundosAtuais);
  }

  tempoPotAtual += "|" + String(nivelPotenciaAtual);
  char3->setValue(tempoPotAtual);

  if (Serial2.available() > 0) {
    String entrada = Serial2.readStringUntil('\n');
    entrada.trim();

    DEBUG_PRINT("Comando recebido: ");
    DEBUG_PRINTLN(entrada);

    if (entrada.startsWith("C")) {
      int potencia, minutos, segundos;
      if (sscanf(entrada.c_str(), "C - %d %d %d", &potencia, &minutos, &segundos) == 3) {
        if ((minutos > 0 || segundos > 0) && (potencia > 0)) {
          iniciar_cozimento(potencia, minutos, segundos);
        }
      } else {
        DEBUG_PRINTLN("Formato de comando inválido.");
      }
    } else if (entrada.startsWith("P")) {
      int novaPotencia;
      if (sscanf(entrada.c_str(), "P - %d", &novaPotencia) == 1) {
        alterar_potencia_para(novaPotencia);
      } else {
        DEBUG_PRINTLN("Formato de comando inválido.");
      }
    } else if (entrada == "S") {
      parar_cozimento();
    } else if (entrada == "N") {
      char status[20];
      sprintf(status, "R - %d %d %d %d", estadoFogao, nivelPotenciaAtual, minutosAtuais, segundosAtuais);
      Serial2.println(status);
      Serial.println("Enviado");
    } else if (entrada == "D") {
      if (estadoFogao == COZINHANDO) {
        desligar_fogao();
      }
    } else if (entrada == "X") {
      if (estadoFogao != COZINHANDO) {
        ESP.restart();
      }
    } else {
      DEBUG_PRINTLN("Comando desconhecido.");
    }
  }

  atualizar_tempo_cozimento();
}

// Função que atualiza o tempo de cozimento e verifica se o tempo acabou
// Esta função atualiza os minutos e segundos restantes para o cozimento
// e desliga o fogão se o tempo de cozimento acabar.
void atualizar_tempo_cozimento() {
  if (estadoFogao == COZINHANDO) {
    unsigned long tempoAtual = millis();
    if (tempoAtual >= tempoFinal) {
      desligar_fogao();
      DEBUG_PRINTLN("Tempo de cozimento terminado. Fogão desligado.");
    } else {
      unsigned long tempoRestante = tempoFinal - tempoAtual;
      minutosAtuais = (tempoRestante / 1000) / 60;
      segundosAtuais = (tempoRestante / 1000) % 60;
    }
  }
}

// Função que simula o aperto de um botão
// Esta função simula o acionamento de um botão por meio de um pulso digital
// no pino especificado.
void apertar_botao(int pino) {
  digitalWrite(pino, LOW);
  delay(250);
  digitalWrite(pino, HIGH);
}

// Função que liga o fogão
// Esta função liga o fogão, atualizando o estado e ajustando a potência inicial.
void ligar_fogao() {
  if (estadoFogao == DESLIGADO) {
    DEBUG_PRINTLN("Ligando fogão!!!");
    apertar_botao(BOTAO_LIGAR);
    estadoFogao = LIGADO;
    nivelPotenciaAtual = NENHUM_NIVEL_SELECIONADO;
    tempoFinal = 0;
  } else {
    DEBUG_PRINTLN("Fogão já está ligado.");
  }
}

// Função que desliga o fogão
// Esta função desliga o fogão, atualizando o estado e resetando os valores de potência e tempo.
void desligar_fogao() {
  if (estadoFogao != DESLIGADO) {
    DEBUG_PRINTLN("Desligando fogão!!!");
    apertar_botao(BOTAO_DESLIGAR);
    estadoFogao = DESLIGADO;
    nivelPotenciaAtual = NENHUM_NIVEL_SELECIONADO;
    minutosAtuais = 0;
    segundosAtuais = 0;
    tempoFinal = 0;
  } else {
    DEBUG_PRINTLN("Fogão já está desligado.");
  }
}

// Função que inicia o cozimento com a potência e o tempo especificados
// Esta função inicia o processo de cozimento configurando a potência e o tempo de cozimento.
void iniciar_cozimento(uint potencia, uint minutos, uint segundos) {
  if (estadoFogao == DESLIGADO) {
    ligar_fogao();
    apertar_botao(BOTAO_ALTERAR_MODO);
    nivelPotenciaAtual = NIVEL_8;
    alterar_potencia_para(potencia);

    minutosIniciais = minutos;
    segundosIniciais = segundos;
    minutosAtuais = minutosIniciais;
    segundosAtuais = segundosIniciais;

    unsigned long tempoTotal = (minutos * 60UL + segundos) * 1000UL;
    tempoFinal = millis() + tempoTotal;

    estadoFogao = COZINHANDO;
  } else {
    DEBUG_PRINTLN("Fogão já está ligado. Desligue-o antes de iniciar um novo cozimento.");
  }
}

// Função que para o cozimento interrompendo o processo e desligando o fogão
// Esta função interrompe o processo de cozimento e desliga o fogão.
void parar_cozimento() {
  if (estadoFogao == COZINHANDO) {
    desligar_fogao();
    DEBUG_PRINTLN("Cozimento interrompido. Fogão desligado.");
  } else {
    DEBUG_PRINTLN("Fogão não está cozinhando.");
  }
}

// Função que altera a potência do fogão para um novo valor especificado
// Esta função ajusta a potência do fogão para o nível especificado.
void alterar_potencia_para(uint novaPotencia) {
  if (estadoFogao == LIGADO || estadoFogao == COZINHANDO) {
    if ((novaPotencia >= NIVEL_1 && novaPotencia <= NIVEL_6) && novaPotencia != nivelPotenciaAtual) {
      while (nivelPotenciaAtual == NENHUM_NIVEL_SELECIONADO) {
        apertar_botao(BOTAO_MENOS_POTENCIA);
        nivelPotenciaAtual++;
        delay(250);
      }

      while (nivelPotenciaAtual > NIVEL_6) {
        apertar_botao(BOTAO_MENOS_POTENCIA);
        nivelPotenciaAtual--;
        delay(250);
      }

      while (nivelPotenciaAtual < novaPotencia) {
        apertar_botao(BOTAO_MAIS_POTENCIA);
        nivelPotenciaAtual++;
        delay(250);
      }

      while (nivelPotenciaAtual > novaPotencia) {
        apertar_botao(BOTAO_MENOS_POTENCIA);
        nivelPotenciaAtual--;
        delay(250);
      }
      DEBUG_PRINT("Potência ajustada para: ");
      DEBUG_PRINTLN(novaPotencia);
    } else {
      DEBUG_PRINTLN("Nível de potência inválido.");
    }
  } else {
    DEBUG_PRINTLN("Fogão está desligado. Ligue o fogão primeiro.");
  }
}
