# Instruções de Instalação

1. **Extração do Arquivo de Dependências:**
   - Extraia o arquivo de dependências dentro do diretório `libraries` localizado na pasta onde o Arduino está instalado.

2. **Nota Importante:**
   - O upload do código para a placa Arduino só funciona em sistemas operacionais Windows.
