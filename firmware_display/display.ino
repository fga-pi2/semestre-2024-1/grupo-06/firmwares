#include <lvgl.h>
#include <TFT_eSPI.h>
#include <XPT2046_Touchscreen.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>

#define TX0 1
#define RX0 3

#define XPT2046_IRQ 36
#define XPT2046_MOSI 32
#define XPT2046_MISO 39
#define XPT2046_CLK 25
#define XPT2046_CS 33

#define LARGURA_TELA 320
#define ALTURA_TELA 240

#define DEBUG 1

#if DEBUG == 1
  #define DEBUG_PRINT(x) Serial.print(x)
  #define DEBUG_PRINTLN(x) Serial.println(x)
#else
  #define DEBUG_PRINT(x)
  #define DEBUG_PRINTLN(x)
#endif

enum ESTADO_FOGAO {
  DESLIGADO = 0,
  LIGADO = 1,
  COZINHANDO = 2
};

SPIClass touchscreenSPI = SPIClass(VSPI);
XPT2046_Touchscreen touchscreen(XPT2046_CS, XPT2046_IRQ);

#define TAMANHO_BUFFER_DESENHO (LARGURA_TELA * ALTURA_TELA / 10 * (LV_COLOR_DEPTH / 8))
uint32_t buffer_desenho[TAMANHO_BUFFER_DESENHO / 4];

int x, y, z;

static int estadoFogao = DESLIGADO;
static int estadoFogaoAnterior = DESLIGADO;
static int nivelPotenciaAtual = 0;

static lv_obj_t *tela_principal = NULL;
static lv_obj_t *abas = NULL;
static lv_obj_t *botao_start = NULL;
static lv_obj_t *botao_stop = NULL;
static lv_obj_t *botao_minutos = NULL;
static lv_obj_t *botao_segundos = NULL;
static lv_obj_t *botao_incremento = NULL;
static lv_obj_t *botao_decremento = NULL;
static lv_obj_t *botao_resetar = NULL;
static lv_obj_t *retangulo_fundo = NULL;
static lv_obj_t *label_potencia = NULL;
static lv_obj_t *fundo_potencia = NULL;
static int minutos = 0;
static int segundos = 0;
static bool desabilitarSerial = false;
static bool timer_executando = false;
static lv_obj_t *botao_selecionado = NULL;
static lv_obj_t *botao_potencia_selecionado = NULL;
static int potencia_selecionada = 0;
static lv_obj_t *botoes_potencia[6];

void leitura_touchscreen(lv_indev_t *indev, lv_indev_data_t *dados);
static void callback_botao_selecao(lv_event_t *e);
static void callback_ajustar_tempo(lv_event_t *e);
static void callback_evento_start(lv_event_t *e);
static void callback_evento_stop(lv_event_t *e);
static void callback_botao_resetar(lv_event_t *e);
static void callback_botao_potencia(lv_event_t *e);
void criar_tela_inicial(void);
void criar_interface_principal(void);
void tarefa_serial(void *pvParameter);
void atualizar_botoes_fogao();

void setup() {
  Serial.begin(115200, SERIAL_8N1, RX0, TX0);
  String LVGL_Arduino = String("LVGL Library Version: ") + lv_version_major() + "." + lv_version_minor() + "." + lv_version_patch();
  DEBUG_PRINTLN(LVGL_Arduino);

  lv_init();

  touchscreenSPI.begin(XPT2046_CLK, XPT2046_MISO, XPT2046_MOSI, XPT2046_CS);
  
  if (!touchscreen.begin(touchscreenSPI)) {
    Serial.println("Touchscreen initialization failed!");
    while (1) delay(10);
  }

  touchscreen.setRotation(3);

  lv_display_t *display = lv_tft_espi_create(LARGURA_TELA, ALTURA_TELA, buffer_desenho, sizeof(buffer_desenho));
  if (display == NULL) {
    Serial.println("Falha ao criar o display!");
    while (1) delay(10);
  }

  lv_indev_t *dispositivo_entrada = lv_indev_create();
  if (dispositivo_entrada == NULL) {
    Serial.println("Falha ao criar o dispositivo de entrada!");
    while (1) delay(10);
  }
  lv_indev_set_type(dispositivo_entrada, LV_INDEV_TYPE_POINTER);
  lv_indev_set_read_cb(dispositivo_entrada, leitura_touchscreen);

  criar_interface_principal();

  xTaskCreatePinnedToCore(tarefa_serial, "Tarefa Serial", 4096, NULL, 1, NULL, 1);
}

void loop() {
  lv_task_handler();
  delay(1);
  lv_tick_inc(1);
}

// Função para leitura do touchscreen
// Esta função realiza a leitura dos dados do touchscreen, mapeia as coordenadas
// e atualiza o estado de entrada do LittlevGL.
void leitura_touchscreen(lv_indev_t *indev, lv_indev_data_t *dados) {
  if (touchscreen.tirqTouched() && touchscreen.touched()) {
    TS_Point p = touchscreen.getPoint();
    x = map(p.x, 200, 3700, 1, LARGURA_TELA);
    y = map(p.y, 240, 3800, 1, ALTURA_TELA);
    z = p.z;

    dados->state = LV_INDEV_STATE_PRESSED;
    dados->point.x = x;
    dados->point.y = y;
  } else {
    dados->state = LV_INDEV_STATE_RELEASED;
  }
}

// Função para atualizar o display do cronômetro
// Esta função atualiza o texto dos labels do cronômetro para mostrar os minutos
// e segundos atuais.
void atualizar_display_cronometro_cb(void *arg) {
  lv_label_set_text_fmt(lv_obj_get_child(botao_minutos, 0), "%02d", minutos);
  lv_label_set_text_fmt(lv_obj_get_child(botao_segundos, 0), "%02d", segundos);
}

static void atualizar_display_cronometro() {
  lv_async_call(atualizar_display_cronometro_cb, NULL);
}

// Função de callback para o botão de seleção
// Esta função gerencia a seleção de botões para ajustar o tempo do cronômetro,
// alternando o estado visual dos botões selecionados.
static void callback_botao_selecao(lv_event_t *e) {
  lv_obj_t *botao = (lv_obj_t *)lv_event_get_target(e);
  if (botao == NULL) {
    Serial.println("Botão nulo na função de callback!");
    return;
  }

  if (botao == botao_selecionado) {
    lv_obj_clear_state(botao_selecionado, LV_STATE_CHECKED);
    lv_obj_set_style_bg_color(botao_selecionado, lv_color_white(), 0);
    botao_selecionado = NULL;
  } else {
    if (botao_selecionado) {
      lv_obj_clear_state(botao_selecionado, LV_STATE_CHECKED);
      lv_obj_set_style_bg_color(botao_selecionado, lv_color_white(), 0);
    }

    botao_selecionado = botao;
    lv_obj_add_state(botao_selecionado, LV_STATE_CHECKED);
    lv_obj_set_style_bg_color(botao_selecionado, lv_color_make(0xe0, 0xe0, 0xe0), 0);
  }
}

// Função de callback para ajuste de tempo
// Esta função ajusta o tempo do cronômetro, incrementando ou decrementando os
// minutos ou segundos conforme o botão selecionado.
static void callback_ajustar_tempo(lv_event_t *e) {
  lv_obj_t *botao = (lv_obj_t *)lv_event_get_target(e);
  if (botao == NULL) {
    Serial.println("Botão nulo na função de callback!");
    return;
  }
  bool incrementar = (botao == botao_incremento);

  if (botao_selecionado == botao_minutos) {
    minutos = (minutos + (incrementar ? 1 : -1) + 60) % 60;
    lv_label_set_text_fmt(lv_obj_get_child(botao_selecionado, 0), "%02d", minutos);
  } else if (botao_selecionado == botao_segundos) {
    segundos = (segundos + (incrementar ? 1 : -1) + 60) % 60;
    lv_label_set_text_fmt(lv_obj_get_child(botao_selecionado, 0), "%02d", segundos);
  }
}

// Função de callback para o evento de início
// Esta função inicia o cronômetro e muda o estado do fogão para COZINHANDO,
// atualizando o estado visual dos botões correspondentes.
static void callback_evento_start(lv_event_t *e) {
  lv_obj_t *botao = (lv_obj_t *)lv_event_get_target(e);
  if (botao == NULL) {
    Serial.println("Botão nulo na função de callback!");
    return;
  }

  if (estadoFogao == DESLIGADO) {
    estadoFogao = COZINHANDO;
    lv_async_call([](void *param) { atualizar_botoes_fogao(); }, NULL);

    lv_obj_set_style_bg_color(botao_stop, lv_color_make(255, 0, 0), 0);
    lv_label_set_text(lv_obj_get_child(botao_stop, 0), LV_SYMBOL_STOP);
    timer_executando = true;

    if (botao_selecionado) {
      lv_obj_clear_state(botao_selecionado, LV_STATE_CHECKED);
      lv_obj_set_style_bg_color(botao_selecionado, lv_color_white(), 0);
      lv_obj_set_style_border_color(botao_selecionado, lv_color_black(), 0);
      botao_selecionado = NULL;
    }

    if (!desabilitarSerial) {
      char dados[20];
      const char *texto_potencia = lv_label_get_text(lv_obj_get_child(botao_potencia_selecionado, 0));
      snprintf(dados, sizeof(dados), "C - %s %d %d", texto_potencia, minutos, segundos);
      Serial.println(dados);
    }
  }
}

// Função de callback para o evento de parada
// Esta função para o cronômetro e muda o estado do fogão para DESLIGADO,
// atualizando o estado visual dos botões correspondentes.
static void callback_evento_stop(lv_event_t *e) {
  lv_obj_t *botao = (lv_obj_t *)lv_event_get_target(e);
  if (botao == NULL) {
    Serial.println("Botão nulo na função de callback!");
    return;
  }

  if (estadoFogao == COZINHANDO) {
    estadoFogao = DESLIGADO;
    lv_async_call([](void *param) { atualizar_botoes_fogao(); }, NULL);

    lv_obj_set_style_bg_color(botao_start, lv_color_make(0, 255, 0), 0);
    lv_label_set_text(lv_obj_get_child(botao_start, 0), LV_SYMBOL_PLAY);
    timer_executando = false;

    if (!desabilitarSerial) {
      Serial.println("S");
    }
  }
}

// Função de callback para o botão de resetar
// Esta função envia um comando "X" pela Serial ao ser acionada.
static void callback_botao_resetar(lv_event_t *e) {
  Serial.println("X");
}

// Função de callback para o botão de potência
// Esta função ajusta a potência do fogão conforme o botão de potência
// selecionado e atualiza o estado visual dos botões correspondentes.
static void callback_botao_potencia(lv_event_t *e) {
  lv_obj_t *botao = (lv_obj_t *)lv_event_get_target(e);
  if (botao == NULL) {
    Serial.println("Botão nulo na função de callback!");
    return;
  }
  int potencia = atoi(lv_label_get_text(lv_obj_get_child(botao, 0)));
  potencia_selecionada = potencia;

  if (estadoFogao == COZINHANDO) {
    if (botao_potencia_selecionado == botao) {
      return;
    }

    if (botao_potencia_selecionado) {
      lv_obj_set_style_border_width(botao_potencia_selecionado, 0, 0);
    }

    botao_potencia_selecionado = botao;
    nivelPotenciaAtual = potencia;

    lv_obj_set_style_border_color(botao_potencia_selecionado, lv_color_make(0, 255, 0), 0);
    lv_obj_set_style_border_width(botao_potencia_selecionado, 3, 0);

    if (!desabilitarSerial) {
      char dados[10];
      sprintf(dados, "P - %d", potencia);
      Serial.println(dados);
    }
  } else {
    if (botao_potencia_selecionado == botao) {
      lv_obj_set_style_border_width(botao_potencia_selecionado, 0, 0);
      botao_potencia_selecionado = NULL;
      nivelPotenciaAtual = 0;
    } else {
      if (botao_potencia_selecionado) {
        lv_obj_set_style_border_width(botao_potencia_selecionado, 0, 0);
      }

      botao_potencia_selecionado = botao;
      nivelPotenciaAtual = potencia;
      lv_obj_set_style_border_color(botao_potencia_selecionado, lv_color_make(0, 255, 0), 0);
      lv_obj_set_style_border_width(botao_potencia_selecionado, 3, 0);
    }
  }
}

// Função para atualizar o estado dos botões do fogão
// Esta função habilita ou desabilita os botões de início e parada
// conforme o estado atual do fogão.
void atualizar_botoes_fogao() {
  if (estadoFogao == COZINHANDO) {
    lv_obj_add_state(botao_start, LV_STATE_DISABLED);
    lv_obj_clear_state(botao_stop, LV_STATE_DISABLED);
  } else {
    lv_obj_clear_state(botao_start, LV_STATE_DISABLED);
    lv_obj_add_state(botao_stop, LV_STATE_DISABLED);
  }
}

// Função para criar a interface principal
// Esta função configura a interface gráfica do fogão inteligente,
// criando e posicionando todos os botões e labels necessários.
void criar_interface_principal(void) {
  lv_obj_t *tela = lv_scr_act();
  if (tela == NULL) {
    Serial.println("Falha ao obter a tela principal!");
    while (1) delay(10);
  }

  lv_obj_set_scrollbar_mode(tela, LV_SCROLLBAR_MODE_OFF);
  lv_obj_clear_flag(tela, LV_OBJ_FLAG_SCROLL_ELASTIC);

  abas = lv_tabview_create(tela);
  if (abas == NULL) {
    Serial.println("Falha ao criar a aba!");
    while (1) delay(10);
  }
  lv_tabview_set_tab_bar_size(abas, 30);
  lv_obj_align(abas, LV_ALIGN_TOP_MID, 0, 0);
  lv_obj_set_scrollbar_mode(lv_tabview_get_content(abas), LV_SCROLLBAR_MODE_OFF);
  lv_obj_clear_flag(lv_tabview_get_content(abas), LV_OBJ_FLAG_SCROLL_ELASTIC);

  lv_obj_t *aba1 = lv_tabview_add_tab(abas, "Fogao Inteligente");
  if (aba1 == NULL) {
    Serial.println("Falha ao adicionar aba Tela Inicial!");
    while (1) delay(10);
  }

  tela_principal = aba1;
  lv_obj_set_scrollbar_mode(tela_principal, LV_SCROLLBAR_MODE_OFF);
  lv_obj_clear_flag(tela_principal, LV_OBJ_FLAG_SCROLL_ELASTIC);
  lv_obj_clear_flag(tela_principal, LV_OBJ_FLAG_SCROLLABLE);

  static lv_style_t estilo;
  lv_style_init(&estilo);
  lv_style_set_text_font(&estilo, &lv_font_montserrat_48);
  lv_style_set_text_color(&estilo, lv_color_black());
  lv_style_set_bg_color(&estilo, lv_color_white());
  lv_style_set_pad_all(&estilo, 0);
  lv_style_set_text_letter_space(&estilo, 2);
  lv_style_set_border_color(&estilo, lv_color_black());
  lv_style_set_border_width(&estilo, 2);

  retangulo_fundo = lv_obj_create(tela_principal);
  if (retangulo_fundo == NULL) {
    Serial.println("Falha ao criar retângulo de fundo!");
    while (1) delay(10);
  }
  lv_obj_set_size(retangulo_fundo, 300, 70);
  lv_obj_align(retangulo_fundo, LV_ALIGN_TOP_MID, 0, 125);
  lv_obj_set_style_radius(retangulo_fundo, 20, 0);
  lv_obj_set_style_bg_color(retangulo_fundo, lv_color_make(240, 240, 240), 0);
  lv_obj_set_style_border_color(retangulo_fundo, lv_color_make(200, 200, 200), 0);
  lv_obj_set_style_border_width(retangulo_fundo, 2, 0);
  lv_obj_clear_flag(retangulo_fundo, LV_OBJ_FLAG_SCROLL_ELASTIC);

  fundo_potencia = lv_obj_create(tela_principal);
  if (fundo_potencia == NULL) {
    Serial.println("Falha ao criar fundo de potência!");
    while (1) delay(10);
  }
  lv_obj_set_size(fundo_potencia, 90, 30);
  lv_obj_set_style_radius(fundo_potencia, 5, 0);
  lv_obj_set_style_bg_color(fundo_potencia, lv_color_make(240, 240, 240), 0);
  lv_obj_set_style_border_color(fundo_potencia, lv_color_make(200, 200, 200), 0);
  lv_obj_set_style_border_width(fundo_potencia, 2, 0);
  lv_obj_align_to(fundo_potencia, retangulo_fundo, LV_ALIGN_OUT_TOP_MID, -90, 5);
  lv_obj_clear_flag(fundo_potencia, LV_OBJ_FLAG_SCROLLABLE);
  lv_obj_clear_flag(fundo_potencia, LV_OBJ_FLAG_SCROLL_ELASTIC);

  label_potencia = lv_label_create(fundo_potencia);
  if (label_potencia == NULL) {
    Serial.println("Falha ao criar label de potência!");
    while (1) delay(10);
  }
  lv_label_set_text(label_potencia, "Potencia");
  lv_obj_center(label_potencia);

  botao_minutos = lv_btn_create(tela_principal);
  if (botao_minutos == NULL) {
    Serial.println("Falha ao criar botão minutos!");
    while (1) delay(10);
  }
  lv_obj_set_size(botao_minutos, 90, 80);
  lv_obj_align(botao_minutos, LV_ALIGN_TOP_MID, -48, -5);
  lv_obj_add_event_cb(botao_minutos, callback_botao_selecao, LV_EVENT_CLICKED, NULL);
  lv_obj_add_style(botao_minutos, &estilo, 0);
  lv_obj_clear_flag(botao_minutos, LV_OBJ_FLAG_SCROLL_ELASTIC);
  lv_obj_t *label_botao_minutos = lv_label_create(botao_minutos);
  if (label_botao_minutos == NULL) {
    Serial.println("Falha ao criar label do botão minutos!");
    while (1) delay(10);
  }
  lv_label_set_text_fmt(label_botao_minutos, "%02d", minutos);
  lv_obj_center(label_botao_minutos);

  botao_segundos = lv_btn_create(tela_principal);
  if (botao_segundos == NULL) {
    Serial.println("Falha ao criar botão segundos!");
    while (1) delay(10);
  }
  lv_obj_set_size(botao_segundos, 90, 80);
  lv_obj_align(botao_segundos, LV_ALIGN_TOP_MID, 48, -5);
  lv_obj_add_event_cb(botao_segundos, callback_botao_selecao, LV_EVENT_CLICKED, NULL);
  lv_obj_add_style(botao_segundos, &estilo, 0);
  lv_obj_clear_flag(botao_segundos, LV_OBJ_FLAG_SCROLL_ELASTIC);
  lv_obj_t *label_botao_segundos = lv_label_create(botao_segundos);
  if (label_botao_segundos == NULL) {
    Serial.println("Falha ao criar label do botão segundos!");
    while (1) delay(10);
  }
  lv_label_set_text_fmt(label_botao_segundos, "%02d", segundos);
  lv_obj_center(label_botao_segundos);

  botao_incremento = lv_btn_create(tela_principal);
  if (botao_incremento == NULL) {
    Serial.println("Falha ao criar botão incremento!");
    while (1) delay(10);
  }
  lv_obj_set_size(botao_incremento, 50, 80);
  lv_obj_align(botao_incremento, LV_ALIGN_TOP_MID, -125, -5);
  lv_obj_set_style_bg_color(botao_incremento, lv_color_make(0, 255, 0), 0);
  lv_obj_add_event_cb(botao_incremento, callback_ajustar_tempo, LV_EVENT_CLICKED, NULL);
  lv_obj_clear_flag(botao_incremento, LV_OBJ_FLAG_SCROLL_ELASTIC);
  lv_obj_t *label_botao_incremento = lv_label_create(botao_incremento);
  if (label_botao_incremento == NULL) {
    Serial.println("Falha ao criar label do botão incremento!");
    while (1) delay(10);
  }
  lv_label_set_text(label_botao_incremento, LV_SYMBOL_PLUS);
  lv_obj_center(label_botao_incremento);
  lv_obj_set_style_text_font(label_botao_incremento, &lv_font_montserrat_26, 0);

  botao_decremento = lv_btn_create(tela_principal);
  if (botao_decremento == NULL) {
    Serial.println("Falha ao criar botão decremento!");
    while (1) delay(10);
  }
  lv_obj_set_size(botao_decremento, 50, 80);
  lv_obj_align(botao_decremento, LV_ALIGN_TOP_MID, 125, -5);
  lv_obj_set_style_bg_color(botao_decremento, lv_color_make(255, 0, 0), 0);
  lv_obj_add_event_cb(botao_decremento, callback_ajustar_tempo, LV_EVENT_CLICKED, NULL);
  lv_obj_clear_flag(botao_decremento, LV_OBJ_FLAG_SCROLL_ELASTIC);
  lv_obj_t *label_botao_decremento = lv_label_create(botao_decremento);
  if (label_botao_decremento == NULL) {
    Serial.println("Falha ao criar label do botão decremento!");
    while (1) delay(10);
  }
  lv_label_set_text(label_botao_decremento, LV_SYMBOL_MINUS);
  lv_obj_center(label_botao_decremento);
  lv_obj_set_style_text_font(label_botao_decremento, &lv_font_montserrat_24, 0);

  botao_resetar = lv_btn_create(tela_principal);
  if (botao_resetar == NULL) {
    Serial.println("Falha ao criar botão resetar!");
    while (1) delay(10);
  }
  lv_obj_set_size(botao_resetar, 50, 40);
  lv_obj_align(botao_resetar, LV_ALIGN_TOP_MID, -10, 80);
  lv_obj_set_style_bg_color(botao_resetar, lv_color_make(0, 0, 255), 0);
  lv_obj_add_event_cb(botao_resetar, callback_botao_resetar, LV_EVENT_CLICKED, NULL);
  lv_obj_clear_flag(botao_resetar, LV_OBJ_FLAG_SCROLL_ELASTIC);
  lv_obj_t *label_botao_resetar = lv_label_create(botao_resetar);
  if (label_botao_resetar == NULL) {
    Serial.println("Falha ao criar label do botão resetar!");
    while (1) delay(10);
  }
  lv_label_set_text(label_botao_resetar, LV_SYMBOL_BLUETOOTH);
  lv_obj_center(label_botao_resetar);
  lv_obj_set_style_text_font(label_botao_resetar, &lv_font_montserrat_24, 0);

  botao_start = lv_btn_create(tela_principal);
  if (botao_start == NULL) {
    Serial.println("Falha ao criar botão start!");
    while (1) delay(10);
  }
  lv_obj_set_size(botao_start, 60, 40);
  lv_obj_align(botao_start, LV_ALIGN_TOP_MID, 50, 80);
  lv_obj_set_style_bg_color(botao_start, lv_color_make(0, 255, 0), 0);
  lv_obj_add_event_cb(botao_start, callback_evento_start, LV_EVENT_CLICKED, NULL);
  lv_obj_clear_flag(botao_start, LV_OBJ_FLAG_SCROLL_ELASTIC);

  lv_obj_t *label_botao_start = lv_label_create(botao_start);
  if (label_botao_start == NULL) {
    Serial.println("Falha ao criar label do botão start!");
    while (1) delay(10);
  }
  lv_label_set_text(label_botao_start, LV_SYMBOL_PLAY);
  lv_obj_center(label_botao_start);
  lv_obj_set_style_text_font(label_botao_start, &lv_font_montserrat_26, 0);

  botao_stop = lv_btn_create(tela_principal);
  if (botao_stop == NULL) {
    Serial.println("Falha ao criar botão stop!");
    while (1) delay(10);
  }
  lv_obj_set_size(botao_stop, 60, 40);
  lv_obj_align(botao_stop, LV_ALIGN_TOP_MID, 115, 80);
  lv_obj_set_style_bg_color(botao_stop, lv_color_make(255, 0, 0), 0);
  lv_obj_add_event_cb(botao_stop, callback_evento_stop, LV_EVENT_CLICKED, NULL);
  lv_obj_clear_flag(botao_stop, LV_OBJ_FLAG_SCROLL_ELASTIC);

  lv_obj_t *label_botao_stop = lv_label_create(botao_stop);
  if (label_botao_stop == NULL) {
    Serial.println("Falha ao criar label do botão stop!");
    while (1) delay 10;
  }
  lv_label_set_text(label_botao_stop, LV_SYMBOL_STOP);
  lv_obj_center(label_botao_stop);
  lv_obj_set_style_text_font(label_botao_stop, &lv_font_montserrat_26, 0);

  lv_color_t cor_botao_potencia = lv_color_make(255, 102, 102);
  for (int i = 0; i < 6; i++) {
    lv_obj_t *botao_potencia = lv_btn_create(tela_principal);
    if (botao_potencia == NULL) {
      Serial.println("Falha ao criar botão de potência!");
      while (1) delay(10);
    }
    botoes_potencia[i] = botao_potencia;
    lv_obj_set_size(botao_potencia, 40, 50);
    lv_obj_set_style_bg_color(botao_potencia, cor_botao_potencia, 0);
    lv_obj_add_event_cb(botao_potencia, callback_botao_potencia, LV_EVENT_CLICKED, NULL);
    lv_obj_align(botao_potencia, LV_ALIGN_TOP_MID, -112 + (i * 45), 135);
    lv_obj_clear_flag(botao_potencia, LV_OBJ_FLAG_SCROLL_ELASTIC);
    lv_obj_t *label_botao_potencia = lv_label_create(botao_potencia);
    if (label_botao_potencia == NULL) {
      Serial.println("Falha ao criar label do botão de potência!");
      while (1) delay(10);
    }
    lv_label_set_text_fmt(label_botao_potencia, "%d", i + 1);
    lv_obj_center(label_botao_potencia);
    lv_obj_set_style_text_font(label_botao_potencia, &lv_font_montserrat_32, 0);
  }

  atualizar_botoes_fogao(); // Atualiza o estado inicial dos botões
}

// Função da tarefa serial
// Esta função é executada como uma tarefa separada no FreeRTOS. Ela
// periodicamente envia um sinal "N" pela Serial e processa comandos
// recebidos para ajustar o estado do fogão e o cronômetro.
void tarefa_serial(void *pvParameter) {
  const unsigned long INTERVALO_ENVIO = 500;
  unsigned long ultimoTempoEnvio = 0;

  while (1) {
    unsigned long tempoAtual = millis();

    if (tempoAtual - ultimoTempoEnvio >= INTERVALO_ENVIO) {
      Serial.println("N");
      ultimoTempoEnvio = tempoAtual;
    }

    if (Serial.available() > 0) {
      String entrada = Serial.readStringUntil('\n');
      
      if (entrada.startsWith("R")) {
        int estadoFogaoRecebido, nivelPotenciaRecebido, minutosRecebidos, segundosRecebidos;
        
        if (sscanf(entrada.c_str(), "R - %d %d %d %d", &estadoFogaoRecebido, &nivelPotenciaRecebido, &minutosRecebidos, &segundosRecebidos) == 4) {
          DEBUG_PRINT("Estado: "); DEBUG_PRINT(estadoFogao);
          DEBUG_PRINT("\tPotência: "); DEBUG_PRINT(nivelPotenciaAtual);
          DEBUG_PRINT("\tMinutos: "); DEBUG_PRINT(minutos);
          DEBUG_PRINT("\tSegundos: "); DEBUG_PRINTLN(segundos);

          if (estadoFogaoRecebido == COZINHANDO) {
            estadoFogao = estadoFogaoRecebido;
            nivelPotenciaAtual = nivelPotenciaRecebido;
            minutos = minutosRecebidos;
            segundos = segundosRecebidos;

            atualizar_display_cronometro();

            bool botaoEncontrado = false;

            for (int i = 0; i < 6; i++) {
              if (botoes_potencia[i] != NULL) {
                const char *texto_botao = lv_label_get_text(lv_obj_get_child(botoes_potencia[i], 0));
                if (atoi(texto_botao) == nivelPotenciaRecebido) {
                  desabilitarSerial = true;
                  lv_obj_send_event(botoes_potencia[i], LV_EVENT_CLICKED, NULL);
                  desabilitarSerial = false;
                  botaoEncontrado = true;
                  break;
                }
              } else {
                DEBUG_PRINTLN("Botão de potência nulo encontrado!");
              }
            }

            if (!botaoEncontrado) {
              DEBUG_PRINTLN("Nenhum botão correspondente à potência encontrada!");
            }

            if (!timer_executando) {
              desabilitarSerial = true;
              lv_obj_send_event(botao_start, LV_EVENT_CLICKED, NULL);
              desabilitarSerial = false;
            }
          } else {
            estadoFogao = estadoFogaoRecebido;
            nivelPotenciaAtual = nivelPotenciaRecebido;

            if (estadoFogaoAnterior == COZINHANDO && estadoFogao == DESLIGADO) {
              Serial.println("S");
            }
          }

          estadoFogaoAnterior = estadoFogao;
          lv_async_call([](void *param) { atualizar_botoes_fogao(); }, NULL);
        } else {
          DEBUG_PRINTLN("Formato de comando inválido");
        }
      } else {
        DEBUG_PRINTLN("Comando não reconhecido: " + entrada);
      }
    }

    vTaskDelay(10 / portTICK_PERIOD_MS);
  }
}
