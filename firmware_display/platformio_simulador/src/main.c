/**
  ******************************************************************************
  * @file    main.c
  * @author  Ac6
  * @version V1.0
  * @date    01-December-2013
  * @brief   Default main function.
  ******************************************************************************
*/

#include "lvgl.h"
#include "app_hal.h"
#include "demos/lv_demos.h"
#include "lv_conf_internal.h"

// Variáveis para o cronômetro
static lv_obj_t *btn;
static lv_obj_t *btn_minutes;
static lv_obj_t *btn_seconds;
static lv_obj_t *btn_increment;
static lv_obj_t *btn_decrement;
static lv_obj_t *btn_reset;
static lv_obj_t *temp_label;
static lv_obj_t *temp_container;
static lv_obj_t *background_rect;
static lv_timer_t *timer = NULL;
static int minutes = 0;
static int seconds = 0;
static bool timer_running = false;
static lv_obj_t *selected_btn = NULL;
static lv_obj_t *selected_red_btn = NULL;
static lv_obj_t *textarea;

// Função para atualizar o display do cronômetro
static void update_timer(lv_timer_t *timer) {
    if (seconds > 0 || minutes > 0) {
        if (seconds == 0) {
            if (minutes > 0) {
                minutes--;
                seconds = 59;
            }
        } else {
            seconds--;
        }
    } else {
        // Parar o cronômetro quando o tempo acabar
        lv_timer_del(timer);
        timer_running = false;
        lv_obj_set_style_bg_color(btn, lv_color_make(0, 255, 0), 0); // Green for Start
        lv_label_set_text(lv_obj_get_child(btn, 0), LV_SYMBOL_PLAY);

        // Habilitar botões de minutos, segundos, incremento e decremento
        lv_obj_clear_state(btn_minutes, LV_STATE_DISABLED);
        lv_obj_clear_state(btn_seconds, LV_STATE_DISABLED);
        lv_obj_clear_state(btn_increment, LV_STATE_DISABLED);
        lv_obj_clear_state(btn_decrement, LV_STATE_DISABLED);
        lv_obj_clear_state(btn_reset, LV_STATE_DISABLED);
    }

    lv_label_set_text_fmt(lv_obj_get_child(btn_minutes, 0), "%02d", minutes);
    lv_label_set_text_fmt(lv_obj_get_child(btn_seconds, 0), "%02d", seconds);
}

// Função de callback para selecionar um botão de tempo
static void btn_select_cb(lv_event_t *e) {
    lv_obj_t *btn = lv_event_get_target(e);

    if (btn == selected_btn) {
        // Desmarcar o botão se ele já estiver selecionado
        lv_obj_clear_state(selected_btn, LV_STATE_CHECKED);
        lv_obj_set_style_bg_color(selected_btn, lv_color_white(), 0);
        selected_btn = NULL;
    } else {
        // Desmarcar o botão anteriormente selecionado
        if (selected_btn) {
            lv_obj_clear_state(selected_btn, LV_STATE_CHECKED);
            lv_obj_set_style_bg_color(selected_btn, lv_color_white(), 0);
        }

        // Marcar o novo botão selecionado
        selected_btn = btn;
        lv_obj_add_state(selected_btn, LV_STATE_CHECKED);
        lv_obj_set_style_bg_color(selected_btn, lv_color_make(0xe0, 0xe0, 0xe0), 0); // Light gray for selected
    }
}

// Função de callback para incrementar/decrementar o tempo
static void btn_adjust_cb(lv_event_t *e) {
    lv_obj_t *btn = lv_event_get_target(e);
    bool increment = (btn == btn_increment);

    if (selected_btn == btn_minutes) {
        minutes = (minutes + (increment ? 1 : -1) + 60) % 60;
        lv_label_set_text_fmt(lv_obj_get_child(selected_btn, 0), "%02d", minutes);
    } else if (selected_btn == btn_seconds) {
        seconds = (seconds + (increment ? 1 : -1) + 60) % 60;
        lv_label_set_text_fmt(lv_obj_get_child(selected_btn, 0), "%02d", seconds);
    }
}

// Função de callback para o botão Start/Stop
static void btn_event_cb(lv_event_t *e) {
    lv_event_code_t code = lv_event_get_code(e);
    lv_obj_t *btn = lv_event_get_target(e);

    if (code == LV_EVENT_CLICKED) {
        if (!timer_running) {
            // Start the timer
            timer = lv_timer_create(update_timer, 1000, NULL);
            lv_obj_set_style_bg_color(btn, lv_color_make(255, 0, 0), 0); // Red for Stop
            lv_label_set_text(lv_obj_get_child(btn, 0), LV_SYMBOL_STOP);
            timer_running = true;

            // Disable minute and second buttons
            lv_obj_add_state(btn_minutes, LV_STATE_DISABLED);
            lv_obj_add_state(btn_seconds, LV_STATE_DISABLED);
            lv_obj_add_state(btn_increment, LV_STATE_DISABLED);
            lv_obj_add_state(btn_decrement, LV_STATE_DISABLED);
            lv_obj_add_state(btn_reset, LV_STATE_DISABLED);

            // Deselecionar o botão selecionado e ajustar seu fundo
            if (selected_btn) {
                lv_obj_clear_state(selected_btn, LV_STATE_CHECKED);
                lv_obj_set_style_bg_color(selected_btn, lv_color_white(), 0); // Set to the same color as the screen
                lv_obj_set_style_border_color(selected_btn, lv_color_black(), 0); // Add a black border
                selected_btn = NULL;
            }
        } else {
            // Stop the timer
            if (timer) {
                lv_timer_del(timer);
                timer = NULL;
            }
            lv_obj_set_style_bg_color(btn, lv_color_make(0, 255, 0), 0); // Green for Start
            lv_label_set_text(lv_obj_get_child(btn, 0), LV_SYMBOL_PLAY);
            timer_running = false;

            // Enable minute and second buttons
            lv_obj_clear_state(btn_minutes, LV_STATE_DISABLED);
            lv_obj_clear_state(btn_seconds, LV_STATE_DISABLED);
            lv_obj_clear_state(btn_increment, LV_STATE_DISABLED);
            lv_obj_clear_state(btn_decrement, LV_STATE_DISABLED);
            lv_obj_clear_state(btn_reset, LV_STATE_DISABLED);
        }
    }
}

// Função de callback para o botão Reset
static void btn_reset_cb(lv_event_t *e) {
    if (timer) {
        lv_timer_del(timer);
        timer = NULL;
    }
    minutes = 0;
    seconds = 0;
    timer_running = false;
    lv_obj_set_style_bg_color(btn, lv_color_make(0, 255, 0), 0); // Green for Start
    lv_label_set_text(lv_obj_get_child(btn, 0), LV_SYMBOL_PLAY);

    lv_label_set_text_fmt(lv_obj_get_child(btn_minutes, 0), "%02d", minutes);
    lv_label_set_text_fmt(lv_obj_get_child(btn_seconds, 0), "%02d", seconds);

    // Enable minute and second buttons
    lv_obj_clear_state(btn_minutes, LV_STATE_DISABLED);
    lv_obj_clear_state(btn_seconds, LV_STATE_DISABLED);
    lv_obj_clear_state(btn_increment, LV_STATE_DISABLED);
    lv_obj_clear_state(btn_decrement, LV_STATE_DISABLED);
    lv_obj_clear_state(btn_reset, LV_STATE_DISABLED);
}

// Função de callback para os botões vermelhos
static void red_btn_select_cb(lv_event_t *e) {
    lv_obj_t *btn = lv_event_get_target(e);

    if (btn == selected_red_btn) {
        // Desmarcar o botão se ele já estiver selecionado
        lv_obj_set_style_border_width(selected_red_btn, 0, 0); // Remove the border
        selected_red_btn = NULL;
    } else {
        // Desmarcar o botão anteriormente selecionado
        if (selected_red_btn) {
            lv_obj_set_style_border_width(selected_red_btn, 0, 0); // Remove the border
        }

        // Marcar o novo botão selecionado
        selected_red_btn = btn;
        lv_obj_set_style_border_color(selected_red_btn, lv_color_make(0, 255, 0), 0); // Green border for selected
        lv_obj_set_style_border_width(selected_red_btn, 3, 0); // Set border width to make it visible
    }
}

// Função de callback para o roller
static void roller_event_cb(lv_event_t *e) {
    lv_obj_t *roller = lv_event_get_target(e);
    char buf[32];
    lv_roller_get_selected_str(roller, buf, sizeof(buf));

    if (strcmp(buf, "Arroz") == 0) {
        lv_textarea_set_text(textarea, "Observacao sobre Arroz");
    } else if (strcmp(buf, "Ovo Frito") == 0) {
        lv_textarea_set_text(textarea, "Observacao sobre Ovo Frito");
    } else if (strcmp(buf, "Feijao") == 0) {
        lv_textarea_set_text(textarea, "Observacao sobre Feijao");
    } else if (strcmp(buf, "Carne") == 0) {
        lv_textarea_set_text(textarea, "Observacao sobre Carne");
    } else if (strcmp(buf, "Frango") == 0) {
        lv_textarea_set_text(textarea, "Observacao sobre Frango");
    } else if (strcmp(buf, "Sopa") == 0) {
        lv_textarea_set_text(textarea, "Observacao sobre Sopa");
    } else if (strcmp(buf, "Agua") == 0) {
        lv_textarea_set_text(textarea, "Observacao sobre Agua");
    } else {
        lv_textarea_set_text(textarea, "");
    }
}

int main(void)
{
    lv_init();
    hal_setup();

    // Criar uma tela
    lv_obj_t *scr = lv_scr_act();

    lv_obj_set_scrollbar_mode(scr, LV_SCROLLBAR_MODE_OFF);

    // Criar um tab view
    lv_obj_t *tabview = lv_tabview_create(scr);
    lv_tabview_set_tab_bar_size(tabview, 30);
    lv_obj_align(tabview, LV_ALIGN_TOP_MID, 0, 0);  // Alinha o tabview ao topo
    lv_obj_remove_flag(lv_tabview_get_content(tabview), LV_OBJ_FLAG_SCROLLABLE);
    //lv_obj_add_flag(lv_tabview_get_content(tabview), LV_OBJ_FLAG_IGNORE_LAYOUT);

    // Criar as tabs
    lv_obj_t *tab1 = lv_tabview_add_tab(tabview, "Normal");
    lv_obj_t *tab2 = lv_tabview_add_tab(tabview, "Modos");

    // Tab Normal
    lv_obj_t *normal_tab_scr = tab1;

    lv_obj_set_scrollbar_mode(normal_tab_scr, LV_SCROLLBAR_MODE_OFF);

    // Criar um estilo para os botões
    static lv_style_t style;
    lv_style_init(&style);
    lv_style_set_text_font(&style, &lv_font_montserrat_48); // Use a larger font
    lv_style_set_text_color(&style, lv_color_black());
    lv_style_set_bg_color(&style, lv_color_white());
    lv_style_set_pad_all(&style, 0);
    lv_style_set_text_letter_space(&style, 2);
    lv_style_set_border_color(&style, lv_color_black());
    lv_style_set_border_width(&style, 2);

    // Criar um retângulo com borda arredondada como fundo
    background_rect = lv_obj_create(normal_tab_scr);
    lv_obj_set_size(background_rect, 300, 70); // Ajuste o tamanho conforme necessário
    lv_obj_align(background_rect, LV_ALIGN_TOP_MID, 0, 125);
    lv_obj_set_style_radius(background_rect, 20, 0); // Borda arredondada
    lv_obj_set_style_bg_color(background_rect, lv_color_make(240, 240, 240), 0); // Cor de fundo
    lv_obj_set_style_border_color(background_rect, lv_color_make(200, 200, 200), 0); // Cor da borda
    lv_obj_set_style_border_width(background_rect, 2, 0);

    // Criar botões para minutos e segundos
    btn_minutes = lv_btn_create(normal_tab_scr);
    lv_obj_set_size(btn_minutes, 90, 80);
    lv_obj_align(btn_minutes, LV_ALIGN_TOP_MID, -48, -5);
    lv_obj_add_event_cb(btn_minutes, btn_select_cb, LV_EVENT_CLICKED, NULL);
    lv_obj_add_style(btn_minutes, &style, 0);
    lv_obj_t *btn_minutes_label = lv_label_create(btn_minutes);
    lv_label_set_text_fmt(btn_minutes_label, "%02d", minutes);
    lv_obj_center(btn_minutes_label);

    btn_seconds = lv_btn_create(normal_tab_scr);
    lv_obj_set_size(btn_seconds, 90, 80);
    lv_obj_align(btn_seconds, LV_ALIGN_TOP_MID, 48, -5);
    lv_obj_add_event_cb(btn_seconds, btn_select_cb, LV_EVENT_CLICKED, NULL);
    lv_obj_add_style(btn_seconds, &style, 0);
    lv_obj_t *btn_seconds_label = lv_label_create(btn_seconds);
    lv_label_set_text_fmt(btn_seconds_label, "%02d", seconds);
    lv_obj_center(btn_seconds_label);

    // Criar botão de incremento
    btn_increment = lv_btn_create(normal_tab_scr);
    lv_obj_set_size(btn_increment, 50, 80);
    lv_obj_align(btn_increment, LV_ALIGN_TOP_MID, -125, -5);
    lv_obj_set_style_bg_color(btn_increment, lv_color_make(0, 255, 0), 0); // Green for Increment
    lv_obj_add_event_cb(btn_increment, btn_adjust_cb, LV_EVENT_CLICKED, NULL);
    lv_obj_t *btn_increment_label = lv_label_create(btn_increment);
    lv_label_set_text(btn_increment_label, LV_SYMBOL_PLUS);
    lv_obj_center(btn_increment_label);
    lv_obj_set_style_text_font(btn_increment_label, &lv_font_montserrat_26, 0); // Fonte maior

    // Criar botão de decremento
    btn_decrement = lv_btn_create(normal_tab_scr);
    lv_obj_set_size(btn_decrement, 50, 80);
    lv_obj_align(btn_decrement, LV_ALIGN_TOP_MID, 125, -5);
    lv_obj_set_style_bg_color(btn_decrement, lv_color_make(255, 0, 0), 0); // Red for Decrement
    lv_obj_add_event_cb(btn_decrement, btn_adjust_cb, LV_EVENT_CLICKED, NULL);
    lv_obj_t *btn_decrement_label = lv_label_create(btn_decrement);
    lv_label_set_text(btn_decrement_label, LV_SYMBOL_MINUS);
    lv_obj_center(btn_decrement_label);
    lv_obj_set_style_text_font(btn_decrement_label, &lv_font_montserrat_24, 0); // Fonte maior

    // Criar botão de reset
    btn_reset = lv_btn_create(normal_tab_scr);
    lv_obj_set_size(btn_reset, 60, 40);
    lv_obj_align(btn_reset, LV_ALIGN_TOP_MID, -5, 80);
    lv_obj_set_style_bg_color(btn_reset, lv_color_make(0, 0, 255), 0); // Blue for Reset
    lv_obj_add_event_cb(btn_reset, btn_reset_cb, LV_EVENT_CLICKED, NULL);
    lv_obj_t *btn_reset_label = lv_label_create(btn_reset);
    lv_label_set_text(btn_reset_label, LV_SYMBOL_REFRESH);
    lv_obj_center(btn_reset_label);
    lv_obj_set_style_text_font(btn_reset_label, &lv_font_montserrat_24, 0); // Fonte maior

    // Criar um retângulo ao redor do campo de temperatura
    temp_container = lv_obj_create(normal_tab_scr);
    lv_obj_set_size(temp_container, 110, 40);
    lv_obj_align(temp_container, LV_ALIGN_TOP_MID, -95, 80);
    lv_obj_set_scrollbar_mode(temp_container, LV_SCROLLBAR_MODE_OFF);
    lv_obj_set_style_radius(temp_container, 10, 0);
    lv_obj_set_style_bg_color(temp_container, lv_color_make(240, 240, 240), 0);
    lv_obj_set_style_border_color(temp_container, lv_color_make(200, 200, 200), 0);
    lv_obj_set_style_border_width(temp_container, 2, 0);

    // Criar um campo de temperatura em Celsius dentro do retângulo
    temp_label = lv_label_create(temp_container);
    lv_obj_center(temp_label);
    lv_label_set_text_fmt(temp_label, "%d °C", 100); // Valor inicial de 100 °C
    lv_obj_set_style_text_font(temp_label, &lv_font_montserrat_30, 0); // Fonte maior

    // Criar um botão para Start/Stop
    btn = lv_btn_create(normal_tab_scr);
    lv_obj_set_size(btn, 120, 40);
    lv_obj_align(btn, LV_ALIGN_TOP_MID, 90, 80);
    lv_obj_set_style_bg_color(btn, lv_color_make(0, 255, 0), 0); // Green for Start
    lv_obj_add_event_cb(btn, btn_event_cb, LV_EVENT_CLICKED, NULL);

    // Adicionar um label ao botão Start/Stop
    lv_obj_t *btn_label = lv_label_create(btn);
    lv_label_set_text(btn_label, LV_SYMBOL_PLAY);
    lv_obj_center(btn_label);
    lv_obj_set_style_text_font(btn_label, &lv_font_montserrat_26, 0); // Fonte maior

    // Criar os 8 botões retangulares com a mesma cor
    lv_color_t button_color = lv_color_make(255, 102, 102); // Cor dos botões
    for (int i = 0; i < 4; i++) {
        lv_obj_t *red_btn = lv_btn_create(normal_tab_scr);
        lv_obj_set_size(red_btn, 50, 50);
        lv_obj_set_style_bg_color(red_btn, button_color, 0);
        lv_obj_add_event_cb(red_btn, red_btn_select_cb, LV_EVENT_CLICKED, NULL);
        lv_obj_align(red_btn, LV_ALIGN_TOP_MID, -105 + (i * 70), 135);
        lv_obj_t *red_btn_label = lv_label_create(red_btn);
        lv_label_set_text_fmt(red_btn_label, "%d", i + 1);
        lv_obj_center(red_btn_label);
        lv_obj_set_style_text_font(red_btn_label, &lv_font_montserrat_32, 0); // Fonte maior
    }

    // Tab Modos
    lv_obj_t *modos_tab_scr = tab2;

    lv_obj_set_scrollbar_mode(modos_tab_scr, LV_SCROLLBAR_MODE_OFF);

    // Criar roller
    lv_obj_t *roller = lv_roller_create(modos_tab_scr);
    lv_roller_set_options(roller,
                        "Selecione\n"
                        "Arroz\n"
                        "Ovo Frito\n"
                        "Feijao\n"
                        "Carne\n"
                        "Frango\n"
                        "Sopa\n"
                        "Agua\n",
                        LV_ROLLER_MODE_INFINITE);
    lv_roller_set_visible_row_count(roller, 4);
    lv_obj_set_size(roller, 140, 100);
    lv_obj_align(roller, LV_ALIGN_TOP_MID, -80, 0);
    lv_roller_set_selected(roller, 0, LV_ANIM_OFF);
    lv_obj_add_event_cb(roller, roller_event_cb, LV_EVENT_VALUE_CHANGED, NULL);

    // Criar área de texto para observações
    textarea = lv_textarea_create(modos_tab_scr);
    lv_obj_set_size(textarea, 300, 80);
    lv_obj_align(textarea, LV_ALIGN_TOP_MID, 0, 105);
    lv_textarea_set_placeholder_text(textarea, "Observacoes");

    // Criar botão Start
    lv_obj_t *btn_start = lv_btn_create(modos_tab_scr);
    lv_obj_set_size(btn_start, 80, 40);
    lv_obj_align(btn_start, LV_ALIGN_TOP_MID, 75, 30);
    lv_obj_set_style_bg_color(btn_start, lv_color_make(0, 255, 0), 0); // Verde
    lv_obj_t *btn_start_label = lv_label_create(btn_start);
    lv_label_set_text(btn_start_label, LV_SYMBOL_PLAY);
    lv_obj_center(btn_start_label);

    hal_loop();
}
