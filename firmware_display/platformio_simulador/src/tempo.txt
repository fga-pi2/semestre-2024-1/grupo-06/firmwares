/**
  ******************************************************************************
  * @file    main.c
  * @author  Ac6
  * @version V1.0
  * @date    01-December-2013
  * @brief   Default main function.
  ******************************************************************************
*/

#include "lvgl.h"
#include "app_hal.h"

#include "demos/lv_demos.h"

static lv_obj_t *label;
static lv_obj_t *btn_start_stop;
static int hours = 0;
static int minutes = 0;
static int seconds = 0;
static lv_timer_t *timer;
static bool timer_running = false;

static void update_label() {
    char buf[16];
    snprintf(buf, sizeof(buf), "%02d:%02d:%02d", hours, minutes, seconds);
    lv_label_set_text(label, buf);
}

static void increment_hours(lv_event_t * e) {
    hours = (hours + 1) % 24;
    update_label();
}

static void decrement_hours(lv_event_t * e) {
    hours = (hours - 1 + 24) % 24;
    update_label();
}

static void increment_minutes(lv_event_t * e) {
    minutes = (minutes + 1) % 60;
    update_label();
}

static void decrement_minutes(lv_event_t * e) {
    minutes = (minutes - 1 + 60) % 60;
    update_label();
}

static void increment_seconds(lv_event_t * e) {
    seconds = (seconds + 1) % 60;
    update_label();
}

static void decrement_seconds(lv_event_t * e) {
    seconds = (seconds - 1 + 60) % 60;
    update_label();
}

static void timer_callback(lv_timer_t * timer) {
    if (seconds > 0) {
        seconds--;
    } else {
        if (minutes > 0) {
            minutes--;
            seconds = 59;
        } else {
            if (hours > 0) {
                hours--;
                minutes = 59;
                seconds = 59;
            } else {
                lv_timer_pause(timer); // Timer reached zero, stop it.
                timer_running = false;
                lv_label_set_text(lv_obj_get_child(btn_start_stop, NULL), "Start");
            }
        }
    }
    update_label();
}

static void start_stop_timer(lv_event_t * e) {
    if (!timer_running) {
        timer = lv_timer_create(timer_callback, 1000, NULL); // Create timer with 1-second period
        timer_running = true;
        lv_label_set_text(lv_obj_get_child(btn_start_stop, NULL), "Stop");
    } else {
        lv_timer_pause(timer);
        timer_running = false;
        lv_label_set_text(lv_obj_get_child(btn_start_stop, NULL), "Start");
    }
}

int main(void)
{
    lv_init();
    hal_setup();

    lv_obj_t *scr = lv_scr_act();

    label = lv_label_create(scr);
    lv_obj_align(label, LV_ALIGN_TOP_MID, 0, 20);
    update_label();

    lv_obj_t *btn_inc_h = lv_btn_create(scr);
    lv_obj_set_size(btn_inc_h, 50, 50);
    lv_obj_align(btn_inc_h, LV_ALIGN_LEFT_MID, -70, -40);
    lv_obj_add_event_cb(btn_inc_h, increment_hours, LV_EVENT_CLICKED, NULL);
    lv_obj_t *label_inc_h = lv_label_create(btn_inc_h);
    lv_label_set_text(label_inc_h, "+H");
    lv_obj_center(label_inc_h);

    lv_obj_t *btn_dec_h = lv_btn_create(scr);
    lv_obj_set_size(btn_dec_h, 50, 50);
    lv_obj_align(btn_dec_h, LV_ALIGN_LEFT_MID, -70, 40);
    lv_obj_add_event_cb(btn_dec_h, decrement_hours, LV_EVENT_CLICKED, NULL);
    lv_obj_t *label_dec_h = lv_label_create(btn_dec_h);
    lv_label_set_text(label_dec_h, "-H");
    lv_obj_center(label_dec_h);

    lv_obj_t *btn_inc_m = lv_btn_create(scr);
    lv_obj_set_size(btn_inc_m, 50, 50);
    lv_obj_align(btn_inc_m, LV_ALIGN_CENTER, -70, -40);
    lv_obj_add_event_cb(btn_inc_m, increment_minutes, LV_EVENT_CLICKED, NULL);
    lv_obj_t *label_inc_m = lv_label_create(btn_inc_m);
    lv_label_set_text(label_inc_m, "+M");
    lv_obj_center(label_inc_m);

    lv_obj_t *btn_dec_m = lv_btn_create(scr);
    lv_obj_set_size(btn_dec_m, 50, 50);
    lv_obj_align(btn_dec_m, LV_ALIGN_CENTER, -70, 40);
    lv_obj_add_event_cb(btn_dec_m, decrement_minutes, LV_EVENT_CLICKED, NULL);
    lv_obj_t *label_dec_m = lv_label_create(btn_dec_m);
    lv_label_set_text(label_dec_m, "-M");
    lv_obj_center(label_dec_m);

    lv_obj_t *btn_inc_s = lv_btn_create(scr);
    lv_obj_set_size(btn_inc_s, 50, 50);
    lv_obj_align(btn_inc_s, LV_ALIGN_RIGHT_MID, -70, -40);
    lv_obj_add_event_cb(btn_inc_s, increment_seconds, LV_EVENT_CLICKED, NULL);
    lv_obj_t *label_inc_s = lv_label_create(btn_inc_s);
    lv_label_set_text(label_inc_s, "+S");
    lv_obj_center(label_inc_s);

    lv_obj_t *btn_dec_s = lv_btn_create(scr);
    lv_obj_set_size(btn_dec_s, 50, 50);
    lv_obj_align(btn_dec_s, LV_ALIGN_RIGHT_MID, -70, 40);
    lv_obj_add_event_cb(btn_dec_s, decrement_seconds, LV_EVENT_CLICKED, NULL);
    lv_obj_t *label_dec_s = lv_label_create(btn_dec_s);
    lv_label_set_text(label_dec_s, "-S");
    lv_obj_center(label_dec_s);

    btn_start_stop = lv_btn_create(scr);
    lv_obj_set_size(btn_start_stop, 100, 50);
    lv_obj_align(btn_start_stop, LV_ALIGN_BOTTOM_MID, 0, -20);
    lv_obj_add_event_cb(btn_start_stop, start_stop_timer, LV_EVENT_CLICKED, NULL);
    lv_obj_t *label_start_stop = lv_label_create(btn_start_stop);
    lv_label_set_text(label_start_stop, "Start");
    lv_obj_center(label_start_stop);

    hal_loop();
}
