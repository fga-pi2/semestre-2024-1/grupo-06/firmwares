/**
  ******************************************************************************
  * @file    main.c
  * @author  Ac6
  * @version V1.0
  * @date    01-December-2013
  * @brief   Default main function.
  ******************************************************************************
*/

#include "lvgl.h"
#include "app_hal.h"
#include "demos/lv_demos.h"

// Variáveis para o timer
static lv_obj_t *btn;
static lv_obj_t *btn_hours;
static lv_obj_t *btn_minutes;
static lv_obj_t *btn_seconds;
static lv_obj_t *btn_increment;
static lv_obj_t *btn_decrement;
static lv_timer_t *timer;
static int hours = 0;
static int minutes = 0;
static int seconds = 0;
static bool timer_running = false;
static lv_obj_t *selected_btn = NULL;

// Função para atualizar o display do timer
static void update_timer(lv_timer_t *timer) {
    seconds++;
    if (seconds >= 60) {
        seconds = 0;
        minutes++;
        if (minutes >= 60) {
            minutes = 0;
            hours++;
        }
    }

    lv_label_set_text_fmt(lv_obj_get_child(btn_hours, 0), "%02d", hours);
    lv_label_set_text_fmt(lv_obj_get_child(btn_minutes, 0), "%02d", minutes);
    lv_label_set_text_fmt(lv_obj_get_child(btn_seconds, 0), "%02d", seconds);
}

// Função de callback para selecionar um botão de tempo
static void btn_select_cb(lv_event_t *e) {
    lv_obj_t *btn = lv_event_get_target(e);

    if (btn == selected_btn) {
        // Desmarcar o botão se ele já estiver selecionado
        lv_obj_clear_state(selected_btn, LV_STATE_CHECKED);
        lv_obj_set_style_bg_color(selected_btn, lv_color_white(), 0);
        selected_btn = NULL;
    } else {
        // Desmarcar o botão anteriormente selecionado
        if (selected_btn) {
            lv_obj_clear_state(selected_btn, LV_STATE_CHECKED);
            lv_obj_set_style_bg_color(selected_btn, lv_color_white(), 0);
        }

        // Marcar o novo botão selecionado
        selected_btn = btn;
        lv_obj_add_state(selected_btn, LV_STATE_CHECKED);
        lv_obj_set_style_bg_color(selected_btn, lv_color_make(0xe0, 0xe0, 0xe0), 0); // Light gray for selected
    }
}

// Função de callback para incrementar/decrementar o tempo
static void btn_adjust_cb(lv_event_t *e) {
    lv_obj_t *btn = lv_event_get_target(e);
    bool increment = (btn == btn_increment);

    if (selected_btn == btn_hours) {
        hours = (hours + (increment ? 1 : -1) + 24) % 24;
        lv_label_set_text_fmt(lv_obj_get_child(selected_btn, 0), "%02d", hours);
    } else if (selected_btn == btn_minutes) {
        minutes = (minutes + (increment ? 1 : -1) + 60) % 60;
        lv_label_set_text_fmt(lv_obj_get_child(selected_btn, 0), "%02d", minutes);
    } else if (selected_btn == btn_seconds) {
        seconds = (seconds + (increment ? 1 : -1) + 60) % 60;
        lv_label_set_text_fmt(lv_obj_get_child(selected_btn, 0), "%02d", seconds);
    }
}

// Função de callback para o botão Start/Stop
static void btn_event_cb(lv_event_t *e) {
    lv_event_code_t code = lv_event_get_code(e);
    lv_obj_t *btn = lv_event_get_target(e);

    if (code == LV_EVENT_CLICKED) {
        if (!timer_running) {
            // Start the timer
            timer = lv_timer_create(update_timer, 1000, NULL);
            lv_obj_set_style_bg_color(btn, lv_color_make(255, 0, 0), 0); // Red for Stop
            lv_label_set_text(lv_obj_get_child(btn, 0), "Stop");
            timer_running = true;

            // Disable hour, minute, and second buttons
            lv_obj_add_state(btn_hours, LV_STATE_DISABLED);
            lv_obj_add_state(btn_minutes, LV_STATE_DISABLED);
            lv_obj_add_state(btn_seconds, LV_STATE_DISABLED);
            lv_obj_add_state(btn_increment, LV_STATE_DISABLED);
            lv_obj_add_state(btn_decrement, LV_STATE_DISABLED);

            // Deselecionar o botão selecionado e ajustar seu fundo
            if (selected_btn) {
                lv_obj_clear_state(selected_btn, LV_STATE_CHECKED);
                lv_obj_set_style_bg_color(selected_btn, lv_color_white(), 0); // Set to the same color as the screen
                lv_obj_set_style_border_color(selected_btn, lv_color_black(), 0); // Add a black border
                selected_btn = NULL;
            }
        } else {
            // Stop the timer
            lv_timer_del(timer);
            lv_obj_set_style_bg_color(btn, lv_color_make(0, 255, 0), 0); // Green for Start
            lv_label_set_text(lv_obj_get_child(btn, 0), "Start");
            timer_running = false;

            // Enable hour, minute, and second buttons
            lv_obj_clear_state(btn_hours, LV_STATE_DISABLED);
            lv_obj_clear_state(btn_minutes, LV_STATE_DISABLED);
            lv_obj_clear_state(btn_seconds, LV_STATE_DISABLED);
            lv_obj_clear_state(btn_increment, LV_STATE_DISABLED);
            lv_obj_clear_state(btn_decrement, LV_STATE_DISABLED);
        }
    }
}

int main(void)
{
    lv_init();
    hal_setup();

    // Criar uma tela
    lv_obj_t *scr = lv_scr_act();

    // Criar um estilo para os botões
    static lv_style_t style;
    lv_style_init(&style);
    lv_style_set_text_font(&style, &lv_font_montserrat_14); // Use a font that looks like 7-segment
    lv_style_set_text_color(&style, lv_color_black());
    lv_style_set_bg_color(&style, lv_color_white());
    lv_style_set_pad_all(&style, 0);
    lv_style_set_text_letter_space(&style, 2);
    lv_style_set_border_color(&style, lv_color_black());
    lv_style_set_border_width(&style, 2);

    // Criar botões para horas, minutos e segundos
    btn_hours = lv_btn_create(scr);
    lv_obj_set_size(btn_hours, 70, 50);
    lv_obj_align(btn_hours, LV_ALIGN_CENTER, -80, 0);
    lv_obj_add_event_cb(btn_hours, btn_select_cb, LV_EVENT_CLICKED, NULL);
    lv_obj_add_style(btn_hours, &style, 0);
    lv_obj_t *btn_hours_label = lv_label_create(btn_hours);
    lv_label_set_text_fmt(btn_hours_label, "%02d", hours);
    lv_obj_center(btn_hours_label);

    btn_minutes = lv_btn_create(scr);
    lv_obj_set_size(btn_minutes, 70, 50);
    lv_obj_align(btn_minutes, LV_ALIGN_CENTER, 0, 0);
    lv_obj_add_event_cb(btn_minutes, btn_select_cb, LV_EVENT_CLICKED, NULL);
    lv_obj_add_style(btn_minutes, &style, 0);
    lv_obj_t *btn_minutes_label = lv_label_create(btn_minutes);
    lv_label_set_text_fmt(btn_minutes_label, "%02d", minutes);
    lv_obj_center(btn_minutes_label);

    btn_seconds = lv_btn_create(scr);
    lv_obj_set_size(btn_seconds, 70, 50);
    lv_obj_align(btn_seconds, LV_ALIGN_CENTER, 80, 0);
    lv_obj_add_event_cb(btn_seconds, btn_select_cb, LV_EVENT_CLICKED, NULL);
    lv_obj_add_style(btn_seconds, &style, 0);
    lv_obj_t *btn_seconds_label = lv_label_create(btn_seconds);
    lv_label_set_text_fmt(btn_seconds_label, "%02d", seconds);
    lv_obj_center(btn_seconds_label);

    // Criar botão de incremento
    btn_increment = lv_btn_create(scr);
    lv_obj_set_size(btn_increment, 100, 50);
    lv_obj_align(btn_increment, LV_ALIGN_CENTER, -100, 70);
    lv_obj_set_style_bg_color(btn_increment, lv_color_make(0, 255, 0), 0); // Green for Increment
    lv_obj_add_event_cb(btn_increment, btn_adjust_cb, LV_EVENT_CLICKED, NULL);
    lv_obj_t *btn_increment_label = lv_label_create(btn_increment);
    lv_label_set_text(btn_increment_label, "Increment");
    lv_obj_center(btn_increment_label);

    // Criar botão de decremento
    btn_decrement = lv_btn_create(scr);
    lv_obj_set_size(btn_decrement, 100, 50);
    lv_obj_align(btn_decrement, LV_ALIGN_CENTER, 100, 70);
    lv_obj_set_style_bg_color(btn_decrement, lv_color_make(255, 0, 0), 0); // Red for Decrement
    lv_obj_add_event_cb(btn_decrement, btn_adjust_cb, LV_EVENT_CLICKED, NULL);
    lv_obj_t *btn_decrement_label = lv_label_create(btn_decrement);
    lv_label_set_text(btn_decrement_label, "Decrement");
    lv_obj_center(btn_decrement_label);

    // Criar um botão para Start/Stop
    btn = lv_btn_create(scr);
    lv_obj_set_size(btn, 100, 50);
    lv_obj_align(btn, LV_ALIGN_CENTER, 0, 140);
    lv_obj_set_style_bg_color(btn, lv_color_make(0, 255, 0), 0); // Green for Start
    lv_obj_add_event_cb(btn, btn_event_cb, LV_EVENT_CLICKED, NULL);

    // Adicionar um label ao botão Start/Stop
    lv_obj_t *btn_label = lv_label_create(btn);
    lv_label_set_text(btn_label, "Start");
    lv_obj_center(btn_label);

    hal_loop();
}
